# Technical Summary

## Introduction <a href="#randomnumbergenerationonpeerplays-howitsdone-howisrnggenerated" id="randomnumbergenerationonpeerplays-howitsdone-howisrnggenerated"></a>

One exciting, powerful, feature of the Peerplays blockchain is its decentralized random number generator (RNG). The key word being 'decentralized'.

A traditional RNG, as the name suggests, generates random numbers. However, the randomness of the numbers is tied to a single authority that generates these numbers. This leaves an opportunity for abuse since any single authority controlling the RNG could theoretically change the 'randomness' without validation.

The Peerplays implementation of an RNG removes this risk as no single authority can ever control the randomness, or outcome; it requires a consensus from all the node operators (Witnesses) of the blockchain.

## So How Does it Work?

Peerplays is based on the Delegated Proof of Stake (DPOS) consensus mechanism, which means that the Witnesses are all elected by the token holders. This is important from an RNG perspective because it requires loyalty, commitment and honesty to get voted in as a Witness. So with Peerplays you get two layers of randomness:

1. Not all Witnesses are block-signing (active) Witnesses; the block-signers have to first be voted in,  and once voted in the blockchain randomly selects which Witnesses are signing blocks, at ten minute intervals.
2. There is the randomness of the number generation itself.&#x20;

### Generating Random Numbers

Random numbers are generated from secret hashes taken from the previous and current block, on the blockchain, which are then combined into a single data stream and encoded using a special algorithm, before being fed into the random number generator as a seed.

With this approach, the hash of blocks, or transactions, is used as the source of randomness. As the hash is deterministic, everyone will get the same result.

### Distributed Ledger Technology (DLT)

Peerplays is a type of distributed ledger. Distributed ledgers use independent computers (the nodes) to record, share and synchronize transactions in their respective electronic ledgers (instead of keeping data centralized as in a traditional ledger). Once data is added it can't be changed.

The immutability of DLT is critical for the RNG because it ensures the authenticity of a random number after it's generated since each node will have exactly the same random number.

## What Does This Mean for NH5050 Lotteries?

For the NH5050 lottery application there is the comfort and assurance of knowing that the engine behind the selection of winning tickets is protected by the two layers of randomness and the immutability offered by the blockchain. The application leverages this by storing the following data on the blockchain:

* Each Ticket ID, and the Entry IDs associated with each ticket. For example, one ticket with ten entries. The RNG then selects a winning ticket from all the ticket entries.
* The User ID of the winning ticket holder. It's not necessary, or even desirable,  to store any additional information about users on the blockchain since that information is available through the application, then linked back to the User ID.
* The winning ticket/entry ID.

As none of this data can ever be changed the winning ticket, and the user associated with it, are assured.

Consider the following example:

Players buy tickets for a lottery that takes place at 8pm, April 30th and tickets are available for purchase from 7pm, April 1st until 7pm, April 30th.&#x20;

After 7PM, April 30th the buying ticket phase is closed, so the protocol proceeds to the next phase which is to 'draw' the winning ticket. This ticket is calculated based on the hash value of the first block accessible for everyone on the blockchain after 8PM, April 30th.&#x20;

So at 7PM, April 30th, no one can predict the hash of a block at 8PM making the winning ticket truly random and completely immutable.

## Fund Distribution

For each lottery there is a distribution of funds, such as to the winning user and beneficiaries, and for some of these transactions there are also fees.

### Funds

Each lottery is funded from:

* Ticket Sales
* Seed Funding. This is any additional funds from third parties, such as the lottery operator, private donors etc. that are added to pre-populate the lottery. Seed funding isn't mandatory.

Collectively this is the Gross Gaming Revenue (GGR)

### Admin and Other Fees

Before any payments are made certain admin fees have to be deducted:

* Licensing fee (Peerplays and Tonk) = 8%
* Other fees including transaction, Stripe, blockchain and bank fees = 5%
* Donation to VFNH by Peerplays and Tonk = 5%

So in total 18% of the GGR goes on fees/donations leaving a net GGR of 82% of the original funds.

### Payments

After admin fees and other fees and donations have been paid the **remaining** GGR is paid out as follows:

* Monthly prize money = 50%, distributed as:
  * Amount awarded to raffle = 50%
  * Amount added to the progressive jackpot = 50%
* Funds raised for beneficiaries = 50%, distributed as:
  * Given to the local detachments (beneficiary) = 50% (split between detachments based on ticket sales)
  * Given to the state department (organization)

### Blockchain Fees

Each lottery is subject to a 2% fee from the **total prize money.** This fee is required for the operation of the blockchain.

### Example

The following example is based on a lottery with total ticket sales of $10,000:

| Balance        |               |           |  % of Total | Description                                                                      |
| -------------- | ------------- | --------- | ----------- | -------------------------------------------------------------------------------- |
| $10,000.00     |               |           |             | Total ticket sales                                                               |
| $0.00          |               |           |             | Seed Funding                                                                     |
| **$10,000.00** |               |           | **100.0%**  | **Gross Gaming Revenue (GGR)**                                                   |
|                | $800.00       |           | 8.0%        | Licensing fee (Peerplays and TONK)                                               |
|                | $500.00       |           | 5.0%        | Transaction fee (may vary) Stripe, Bank fees, Blockchain (2% of the prize money) |
| **$1,300.00**  |               |           | **13.0%**   | **Total admin fees**                                                             |
| $500.00        |               |           | 5.0%        | Donated to VFNH by Peerplays/Tonk Group                                          |
| **$8,200.00**  |               |           | **82.0%**   | **Net GGR**                                                                      |
|                | **$4,100.00** |           | **41.0%**   | **Monthly prize money (50% of Net GGR)**                                         |
|                |               | $2,050.00 | 20.5%       | Amount awarded to the raffle winner (50% of monthly prize money)                 |
|                |               | $2,050.00 | 20.5%       | Amount added to the progressive jackpot (50% of monthly prize money)             |
|                | **$4,100.00** |           | **41.0%**   | **Total funds raised for beneficiaries**                                         |
|                |               | $3,690.00 | 36.9%       | Goes to the local detachments (90% of total funds raised )                       |
|                |               | $410.00   | 4.1%        | Goes to the state department (10% of total funds raised)                         |
|                |               | 82.00     | 0.82%       | Blockchain fee (2% of Monthly Prize Money)                                       |

## Revenue Over Time

The following example shows potential revenue over a few months. It is based on a $10,000 funded lottery as well.&#x20;

![](<.gitbook/assets/Screen Shot 2020-04-02 at 3.33.43 PM.png>)
